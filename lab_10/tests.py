from django.test import TestCase, Client
from django.urls import resolve
from lab_10 import views
from .models import RegisterSubscribe

import json

# Test Driven Development Test lab_10
class Lab10TestCase(TestCase):

    # check if url OK status for subscribe page
    def test_url_used_10(self):
        response = Client().get('/subscribe/')
        self.assertEqual(response.status_code , 200)

    # check if func in views.py lab_10 used
    def test_func_used_10(self):
        response = resolve('/subscribe/')
        self.assertEqual(response.func , views.subscribe)

    # check if template html lab_10 used
    def test_template_used_10(self):
        response = Client().get('/subscribe/')
        self.assertTemplateUsed(response,'subscribe_page.html')

    # check if user can create account
    def test_user_create_account(self):
        response = RegisterSubscribe.objects.create(nama="bintang", email="bintangagung89@gmail.com",password="123456")
        count_account = RegisterSubscribe.objects.all().count()
        self.assertEqual(count_account,1)

    # check validate user account
    def test_validate_function(self):
        response = resolve('/validate/')
        self.assertEqual(response.func , views.user_validate)

    # check validate email account
    def test_name_valid(self):
        data = {

            "nama": "test",
            "email": "asdasdasdsadas",
            "password": "dasdasdas"
        }
        response = Client().post('/validate/', data)
        json_result = json.loads(response.content.decode('utf-8'))
        self.assertEqual(json_result['message'], 'Name must be 5 to 30 characters or fewer.')

    # check validate password account
    def test_email_valid(self):
        data = {

            "nama": "test",
            "email": "wkwkwk@gmail.com",
            "password": "sdasd"
        }
        response = Client().post('/validate/' ,data)
        json_result = json.loads(response.content.decode('utf-8'))
        self.assertEqual(json_result['message'], 'Name must be 5 to 30 characters or fewer.')