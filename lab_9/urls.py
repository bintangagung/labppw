from django.urls import path
from lab_9 import views


urlpatterns = [
    path('', views.show_books, name="show_books"),
    path('data/',views.data_books,name="data_books")
]