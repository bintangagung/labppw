from django.test import TestCase, Client
from django.urls import resolve
from .views import data_books, show_books
from lab_9 import views

from django.utils import timezone
from django.http import HttpRequest,HttpResponse


# Create your tests here.

class Lab9TestCase(TestCase):

    def test_url_is_exist(self):
        response = Client().get('/books/')
        self.assertEqual(response.status_code,200)

    def test_url_is_not_exist(self):
        response = Client().get('/bookbook/')
        self.assertEqual(response.status_code,404)

    def test_template_used(self):
        response = Client().get('/books/')
        self.assertTemplateUsed(response,'index_books.html')

    # def test_lab_9_func(self):
    #     response_func = resolve('/books/')
    #     self.assertEqual(response_func.func, views.show_books)

    # def test_lab_9_books_json(self):
    #     response_func = resolve('/books/data/')
    #     self.assertEqual(response_func.func,views.data_books)