from django.shortcuts import render
from django.shortcuts import redirect
from django.core import serializers
from django.http import HttpResponse, HttpRequest, HttpResponseBadRequest
import requests
import json
# Create your views here.

search = "quilting"
# show books html page views
def show_books(request):
     if request.method == "POST":
         retrieve_endpoint = request.POST.get('type_books')
         global search # change global bam
         search = retrieve_endpoint
         return redirect('show_books')
     else:
        return render(request, 'index_books.html')

# data buku json
def data_books(request):
    key = search
    book_api = 'https://www.googleapis.com/books/v1/volumes?q='+key
    get_json_obj = requests.get(book_api)
    result = (get_json_obj.json())['items']
    return_data = {"data": result } # data tabel
    json_data = json.dumps(return_data)
    return HttpResponse(json_data, content_type="application/json")
