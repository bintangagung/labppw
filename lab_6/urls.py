from django.urls import path
from lab_6 import views

urlpatterns = [
    path('', views.add_message , name="add_status"),
    path('profil/', views.profil , name="profil"),
    path('index/', views.index , name="index"),
]