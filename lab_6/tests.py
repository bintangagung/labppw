from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import add_message
from .models import Message
from .forms import MessageForm

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Create your tests here.
class Lab6UnitTest(TestCase):

    def test_lab_6_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_lab_6_url_profil(self):
        response = Client().get('/profil/')
        self.assertEqual(response.status_code, 200)

    def test_lab_6_url_index(self):
        response = Client().get('/index/')
        self.assertEqual(response.status_code, 200)

    def test_lab6_using_add_message(self):
        found = resolve('/')
        self.assertEqual(found.func, add_message)

    def test_model_can_create_new_todo(self):
        # Creating a new activity
        new_activity = Message.objects.create(message='halo apa kabar?')

        # Retrieving all available activity
        counting_all_available_Message = Message.objects.all().count()
        self.assertEqual(counting_all_available_Message, 1)


    def test_form_validation_for_blank_items(self):
        form = MessageForm(data={'title': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['message'],
            ["This field is required."]
        )

    def test_lab6_post_success_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('', {'message': test})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_lab6_post_error_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('', {'message': ''})
        self.assertEqual(response_post.status_code, 200)

        response= Client().get('')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)


# Create your tests here.
class Lab6FunctionalTest(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.selenium.implicitly_wait(25)
        super(Lab6FunctionalTest,self).setUp()
        
    def tearDown(self):
        self.selenium.quit()
        super(Lab6FunctionalTest, self).tearDown()

    def test_input_todo(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('https://binbintangagung.herokuapp.com/')
        # find the form element
        title = selenium.find_element_by_id('id_message')
        submit = selenium.find_element_by_name('button')

        # Fill the form with data
        title.send_keys('Coba-coba')

        # submitting the form
        submit.send_keys(Keys.RETURN)

    #challange function test
    def test_title(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('https://binbintangagung.herokuapp.com/')
        # find the title
        title = selenium.title
        self.assertEqual(title, 'Selamat datang')

    def test_button(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('https://binbintangagung.herokuapp.com/')
        # find the button
        button = selenium.find_element_by_name('button')
        self.assertEqual(button.text, 'Submit')

    def test_status_element(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('https://binbintangagung.herokuapp.com/')
        
        element = selenium.find_element_by_class_name('status')
        font = element.value_of_css_property('position')
        self.assertEqual(font, 'static')

    def test_status_message_element(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('https://binbintangagung.herokuapp.com')
        
        element = selenium.find_element_by_class_name('list-status-message')
        font = element.value_of_css_property('text-align')
        self.assertEqual(font, 'left')



    # def test_status_panel_element(self): ERROR GARA_GARA ngga bisa akses semua page karena ada progress bar yang harus di klik
    #     selenium = self.selenium
    #     # Opening the link we want to test
    #     selenium.get('https://binbintangagung.herokuapp.com/profil/')
        
    #     element = selenium.find_element_by_class_name('panel')
    #     bg = element.value_of_css_property('background-color')
    #     self.assertEqual(bg, 'rgba(255, 255, 255, 1)')