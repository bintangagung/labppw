from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render, redirect
from django.contrib import messages
from .models import Message
from .forms import MessageForm
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage

# Create your views here. 

# POST method add_message


def add_message(request):
    form_message = MessageForm(request.POST)

    if form_message.is_valid():
        form_message.save()
        return redirect('add_status')

    else:
        form_message = MessageForm()

    message_status_list = Message.objects.all().order_by('-created_at')  # sorted by creation datetime
    paginator = Paginator(message_status_list, 5)
    page = request.GET.get('page')
    message_status_paginate = paginator.get_page(page)
    return render(request, "index.html", {'message_form': form_message, "message_status_list": message_status_paginate})

def profil(request):
	return render(request,"profil.html")

def index(request):
    return render(request,"index.html")